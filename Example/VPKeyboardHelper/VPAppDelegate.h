//
//  VPAppDelegate.h
//  VPKeyboardHelper
//
//  Created by Vlaho on 08/16/2015.
//  Copyright (c) 2015 Vlaho. All rights reserved.
//

@import UIKit;

@interface VPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

//
//  VPViewController.m
//  VPKeyboardHelper
//
//  Created by Vlaho on 08/16/2015.
//  Copyright (c) 2015 Vlaho. All rights reserved.
//

#import "VPViewController.h"
#import <VPKeyboardHelper/VPKeyboardHelper.h>

@interface VPViewController ()

@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;

@end

@implementation VPViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self kh_configureForKeyboardWithScrollView:self.scrollView];
}



@end

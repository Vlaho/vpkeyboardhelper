//
//  main.m
//  VPKeyboardHelper
//
//  Created by Vlaho on 08/16/2015.
//  Copyright (c) 2015 Vlaho. All rights reserved.
//

@import UIKit;
#import "VPAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([VPAppDelegate class]));
    }
}

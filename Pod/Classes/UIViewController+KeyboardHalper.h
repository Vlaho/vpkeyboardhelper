//
//  UIViewController+VPKeyboardHalper.h
//  KeyboardHelper
//
//  Created by Vlaho Poluta on 8/16/15.
//  Copyright (c) 2015 Infinum. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (VPKeyboardHalper)

- (void)kh_configureForKeyboardWithScrollView:(UIScrollView *)scrollView;

- (void)kh_keyboardWillBeShown:(NSNotification*)aNotification;
- (void)kh_keyboardWillBeHidden:(NSNotification*)aNotification;

@end

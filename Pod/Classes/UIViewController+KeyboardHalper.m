//
//  UIViewController+KeyboardHalper.m
//  KeyboardHelper
//
//  Created by Vlaho Poluta on 8/16/15.
//  Copyright (c) 2015 Infinum. All rights reserved.
//

#import "UIViewController+KeyboardHalper.h"
#import <objc/runtime.h>
#import "KeyboardDeallocationItem.h"

@interface UIViewController (KeyboardHalperPrivate)

@property (nonatomic, weak) UIScrollView *kh_scrollView;
@property (nonatomic, strong) KeyboardDeallocationItem *kh_deallocItem;

@end

@implementation UIViewController (KeyboardHalperPrivate)

@dynamic kh_scrollView;
@dynamic kh_deallocItem;

-(UIScrollView *)kh_scrollView
{
    return objc_getAssociatedObject(self, @selector(kh_scrollView));
}

-(void)setKh_scrollView:(UIScrollView *)kh_scrollView
{
    objc_setAssociatedObject(self, @selector(kh_scrollView), kh_scrollView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(KeyboardDeallocationItem *)kh_deallocItem
{
    return objc_getAssociatedObject(self, @selector(kh_deallocItem));
}

-(void)setKh_deallocItem:(KeyboardDeallocationItem *)kh_deallocItem
{
    objc_setAssociatedObject(self, @selector(kh_deallocItem), kh_deallocItem, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end

@implementation UIViewController (VPKeyboardHalper)

-(void)kh_configureForKeyboardWithScrollView:(UIScrollView *)scrollView
{
    self.kh_scrollView = scrollView;
    self.kh_deallocItem = [KeyboardDeallocationItem deallocItemWithViewController:self];
}

- (void)kh_keyboardWillBeShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    self.kh_scrollView.contentInset = UIEdgeInsetsMake(0, 0, kbSize.height, 0);
    self.kh_scrollView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, kbSize.height, 0);
}

- (void)kh_keyboardWillBeHidden:(NSNotification*)aNotification
{
    self.kh_scrollView.contentInset = UIEdgeInsetsZero;
    self.kh_scrollView.scrollIndicatorInsets = UIEdgeInsetsZero;
}
@end

//
//  KeyboardDeallocationItem.m
//  KeyboardHelper
//
//  Created by Vlaho Poluta on 8/16/15.
//  Copyright (c) 2015 Infinum. All rights reserved.
//

#import "UIViewController+KeyboardHalper.h"
#import "KeyboardDeallocationItem.h"

@interface KeyboardDeallocationItem ()

@property (nonatomic, weak) UIViewController *masterViewController;

@end

@implementation KeyboardDeallocationItem

+(instancetype)deallocItemWithViewController:(UIViewController *)viewController
{
    KeyboardDeallocationItem *item = [KeyboardDeallocationItem new];
    item.masterViewController = viewController;
    return item;
}

-(instancetype)init
{
    self = [super init];
    if (self) {
        [self kh_registerForKeyboardNotifications];
    }
    return self;
}

- (void)kh_registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(kh_keyboardWillBeShown:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(kh_keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)kh_keyboardWillBeShown:(NSNotification*)aNotification
{
    [self.masterViewController kh_keyboardWillBeShown:aNotification];
}

- (void)kh_keyboardWillBeHidden:(NSNotification*)aNotification
{
    [self.masterViewController kh_keyboardWillBeHidden:aNotification];
}

@end

//
//  KeyboardDeallocationItem.h
//  KeyboardHelper
//
//  Created by Vlaho Poluta on 8/16/15.
//  Copyright (c) 2015 Infinum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface KeyboardDeallocationItem : NSObject

+(instancetype)deallocItemWithViewController:(UIViewController *)viewController;

@end

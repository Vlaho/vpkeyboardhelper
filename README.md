# VPKeyboardHelper

[![CI Status](http://img.shields.io/travis/Vlaho/VPKeyboardHelper.svg?style=flat)](https://travis-ci.org/Vlaho/VPKeyboardHelper)
[![Version](https://img.shields.io/cocoapods/v/VPKeyboardHelper.svg?style=flat)](http://cocoapods.org/pods/VPKeyboardHelper)
[![License](https://img.shields.io/cocoapods/l/VPKeyboardHelper.svg?style=flat)](http://cocoapods.org/pods/VPKeyboardHelper)
[![Platform](https://img.shields.io/cocoapods/p/VPKeyboardHelper.svg?style=flat)](http://cocoapods.org/pods/VPKeyboardHelper)

## Usage

To avoid adding / removing listeners to keyboard show / hide and repetitively writing methods for adding / removing scroll view and scroll indicator insets call this simple method:

```ObjC
-(void)kh_configureForKeyboardWithScrollView:(UIScrollView *)scrollView;
```

## Installation

VPKeyboardHelper is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "VPKeyboardHelper"
```

```ruby
source 'https://bitbucket.org/infinum_hr/cocoapods.git'
```

To work with other pods also add:

```ruby
source 'https://github.com/CocoaPods/Specs.git'
```

## Author

Vlaho, vlaho.poluta@infinum.hr

## License

VPKeyboardHelper is available under the MIT license. See the LICENSE file for more info.
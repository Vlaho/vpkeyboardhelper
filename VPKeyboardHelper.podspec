#
# Be sure to run `pod lib lint VPKeyboardHelper.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "VPKeyboardHelper"
  s.version          = "0.1.4"
  s.summary          = "To help users, so they don't have to write code for keyboard showing and hiding"

  s.description      = <<-DESC
                        Add one spimple line to add and remove insets to scroll view and scroll indicatior.
                        Allso you can override those methoeds to add more behaviour to your code.
                       DESC

  s.homepage         = "https://bitbucket.org/Vlaho/vpkeyboardhelper"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "Vlaho" => "vlaho.poluta@infinum.hr" }
  s.source           = { :git => "git@bitbucket.org:Vlaho/vpkeyboardhelper.git", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.platform     = :ios, '7.1'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'

  s.public_header_files = 'Pod/Classes/**/*.h'
  s.frameworks = 'UIKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
